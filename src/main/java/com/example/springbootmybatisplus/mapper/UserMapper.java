package com.example.springbootmybatisplus.mapper;

import com.example.springbootmybatisplus.bean.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lfh
 * @since 2022-11-19
 */
//@Mapper 主配置类上标注了@MapperScan("com.example.springbootmybatisplus.mapper")此处这个可以省略
public interface UserMapper extends BaseMapper<User> {

}
