package com.example.springbootmybatisplus.service;

import com.example.springbootmybatisplus.bean.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lfh
 * @since 2022-11-19
 */
public interface UserService extends IService<User> {

}
